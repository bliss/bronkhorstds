#!/usr/bin/env python

from setuptools import setup

console_scripts_entry_points = [
    "FlowBus = bronkhorst.server:main",
]

setup(
    name="bronkhorst.server",
    version="0.0.0",
    description="A Tango device server for the unified control of Bronkhorst systems",
    url="https://gitlab.esrf.fr/debionne/bronkhorstds",
    entry_points={"console_scripts": console_scripts_entry_points},
)

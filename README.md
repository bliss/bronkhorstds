# Bronkhorst Tango DS for FLOW-BUS

FLOW-BUS is a fieldbus, designed by Bronkhorst High-Tech BV, based on RS485 technology, for digital communication between digital devices, offering the possibility of host-control by PC.

### Installation

```
conda install -c esrf-bcu bronkhorst-flowbus-ds
```

### Tango Installation

Register a new Device server with Jive`s server wizard.

Runs the device with

``` 
FlowBus <instance name>
```

Add a new `FlowBusMaster` class. Fill the properties:


| Property | Value |
|----------|-------|
| `beamline` | `id10` the domain part of the subdevice's tango fullname |
| `comm`     | `serial | tcp` (used as the domain of the subdevice's fullname) |
| `port`     | `COM1` / `/dev/tty1` (for local serial communication) |
| `baudrate` | `38400` (for local serial communication) |
| `host`     | `brainbox.esrf.fr` (for RAW TCP communication) |
| `port`     | `9001` (for RAW TCP communication) |

This `FlowBusMaster` device will discover all the devices on the bus and spawn `FlowBus` subdevices for each nodes:

```
id10/flowbus/snm22219161b
id10/flowbus/snm23205135a
id10/flowbus/snm22219161d
```

The `domain/family/member` is the tango identifier where `domain` is the `beamline` property filled in before and `member` is the `idn` of the node.

from tango.server import run

from bronkhorst.flowbus import FlowBusMaster, FlowBus
from leidenprobemicroscopy.panel import GasFrontPanel


def main():
    run(
        (
            GasFrontPanel,
            FlowBusMaster,
            FlowBus,
        )
    )


if __name__ == "__main__":
    main()

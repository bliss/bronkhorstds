# -*- coding: utf-8 -*-
# FlowBus Tango device server.

import json
import logging
import enum
from serial.serialutil import SerialException

from tango.server import Device, attribute, command, device_property, AttrWriteType
from tango import DbDevInfo, DevState, DevFailed
from tango import Database, Util

import propar

from .tcp import RawTCP

# Get loggers
_logger = logging.getLogger(__name__)


class Comm(enum.Enum):
    Serial = "serial"
    RawTCP = "tcp"


class FlowBus(Device):
    """FlowBus device"""

    comm = device_property(
        dtype=str,
        default_value="serial",
        doc="The communication protocol [serial, tcp]",
    )
    comport = device_property(
        dtype=str, default_value="COM1", doc="The serial port [COM1, /dev/ttyS1]"
    )
    baudrate = device_property(
        dtype=int, default_value=38400, doc="Baud rate such as 9600 or 115200"
    )
    hostname = device_property(
        dtype=str, default_value="brainbox.esrf.fr", doc="The TCP/IP hostname"
    )
    port = device_property(dtype=int, default_value="9001", doc="The TCP/IP port")
    address = device_property(dtype=int, default_value=1, doc="Flowbus address")
    channel = device_property(dtype=int, default_value=1, doc="Flowbus channel")

    def init_device(self):
        super(Device, self).init_device()
        self.info_stream(
            f"Configuration: comport: {self.comport} - baudrate: {self.baudrate} - address: {self.address} - channel: {self.channel}"
        )

        # Connect to an instrument by specifying the channel number to connect to
        if self.comm == Comm.Serial.value:
            self.info_stream(
                f"Configuration: comport={self.comport} baudrate={self.baudrate}"
            )

            self.__instrument = propar.instrument(
                comport=self.comport,
                baudrate=self.baudrate,
                address=self.address,
                channel=self.channel,
            )
        elif self.comm == Comm.RawTCP.value:
            self.info_stream(
                f"Configuration: hostname={self.hostname} port={self.port}"
            )

            # Connect to an instrument by specifying the channel number to connect to
            self.__instrument = propar.instrument(
                comport=self.hostname,
                baudrate=self.port,
                address=self.address,
                channel=self.channel,
                serial_class=RawTCP,
            )
        else:
            raise RuntimeError("Unsupported communication protocol [serial, tcp]")

        self.set_state(DevState.ON)

    # @DebugIt
    @attribute(
        label="idn",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Reads the IDN/SN parameter of the instrument",
    )
    def idn(self):
        # The ID corresponds to DDE parameter 1, which includes the device type (the first byte) and serial number starting with SN.
        return self.__instrument.id[3:]

    @attribute(
        label="type",
        dtype=int,
        access=AttrWriteType.READ,
        doc="Reads the sensor type of the instrument",
    )
    def type(self):
        return self.__instrument.readParameter(22)

    @attribute(
        label="fluid",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Reads the fluid name of the instrument",
    )
    def fluid(self):
        return self.__instrument.readParameter(25).strip()

    @attribute(
        label="measure",
        dtype=float,
        access=AttrWriteType.READ,
        doc="Measured value for direct reading (in capunits, max.= capacity)",
    )
    def measure(self):
        return self.__instrument.readParameter(205)

    @attribute(
        label="setpoint",
        dtype=float,
        access=AttrWriteType.READ_WRITE,
        doc="Wanted value for direct reading (in capunits, max.= capacity)",
    )
    def setpoint(self):
        return self.__instrument.readParameter(206)

    @setpoint.write
    def setpoint(self, value):
        self.__instrument.writeParameter(206, value)

    @attribute(
        label="pressure_inlet",
        dtype=float,
        access=AttrWriteType.READ,
        doc="pressure inlet (upstream) of fluid in bara (for first fluidnr only)",
    )
    def pressure_inlet(self):
        return self.__instrument.readParameter(178)

    @attribute(
        label="pressure_outlet",
        dtype=float,
        access=AttrWriteType.READ,
        doc="Pressure outlet (downstream) of fluid in bara (for first fluidnr only)",
    )
    def pressure_outlet(self):
        return self.__instrument.readParameter(179)

    @attribute(
        label="orifice",
        dtype=float,
        access=AttrWriteType.READ,
        doc="Orifice diameter in mm",
    )
    def orifice(self):
        return self.__instrument.readParameter(180)

    @attribute(
        label="fluid_temperature",
        dtype=float,
        access=AttrWriteType.READ,
        doc="Temperature of fluid through instrument (for first fluidnr only)",
    )
    def fluid_temperature(self):
        return self.__instrument.readParameter(181)

    # @DebugIt
    @command(dtype_in=int, doc_in="Wink duration 1-9 seconds")
    def wink(self, duration):
        self.info_stream(f"Wink: {duration}")
        assert duration > 0 and duration < 10
        return self.__instrument.wink(duration)

    @command(
        dtype_in=int,
        dtype_out=str,
        doc_in="Read a single parameter indicated by DDE nr",
        doc_out="JSON encoded parameter value",
    )
    def read_parameter(self, dde_nr):
        self.info_stream(f"Reading parameter: {dde_nr}")
        assert dde_nr > 0
        param = self.__instrument.db.get_parameter(dde_nr)
        param["value"] = self.__instrument.readParameter(dde_nr)
        return json.dumps(param)

    # @DebugIt
    @command(dtype_in=int, doc_in="Write a single parameter indicated by DDE nr")
    def write_parameter(self, dde_nr):
        self.info_stream(f"Writing parameter: {dde_nr}")
        assert dde_nr > 0
        self.__instrument.writeParameter(dde_nr)


class FlowBusMaster(Device):
    """FlowBus master device"""

    beamline = device_property(
        dtype=str,
        default_value="id00",
        doc="The beamline uid used as domain in domain/family/member",
    )
    comm = device_property(
        dtype=str,
        default_value="serial",
        doc="The communication protocol [serial, tcp]",
    )
    comport = device_property(
        dtype=str, default_value="COM1", doc="The serial port [COM1, /dev/ttyS1]"
    )
    baudrate = device_property(
        dtype=int, default_value=38400, doc="Baud rate such as 9600 or 115200"
    )
    hostname = device_property(
        dtype=str, default_value="brainbox.esrf.fr", doc="The TCP/IP hostname"
    )
    port = device_property(dtype=int, default_value="9001", doc="The TCP/IP port")

    def init_device(self):
        super(Device, self).init_device()

        # Connect to an instrument by specifying the channel number to connect to
        if self.comm == Comm.Serial.value:
            # Use propar communication (python serial module).
            self.info_stream(
                f"Configuration: comport={self.comport} baudrate={self.baudrate}"
            )
            try:
                self.__instrument = propar.instrument(
                    comport=self.comport, baudrate=self.baudrate
                )
            except SerialException as df_err:
                raise RuntimeError(f"Cannot open serial {self.comport}") from df_err

            self.info_stream(f"OK, {self.comport} open")

        elif self.comm == Comm.RawTCP.value:
            self.info_stream(
                f"Configuration: hostname={self.hostname} port={self.port}"
            )

            # Let propar lib to use provided serial class (our own RawTCP).
            # This allow to use serial to ethernet devices (like brainbox).
            self.__instrument = propar.instrument(
                comport=self.hostname, baudrate=self.port, serial_class=RawTCP
            )
            self.info_stream(f"OK, {self.hostname}:{self.port} open")
        else:
            raise RuntimeError("Unsupported communication protocol [serial, tcp]")

        self.info_stream("Trying to communicate with bronkhorst...")

        util = Util.instance()
        db = util.get_database()
        self.__subdevices = []

        try:
            nodes = self.__instrument.master.get_nodes()
        except Exception as gn_err:
            raise RuntimeError(
                "Exception Cannot connect to master node (check it is plugged and ON)"
            ) from gn_err

        self.debug_stream(f"Nodes: {nodes}")
        for node in nodes:
            # The ID corresponds to DDE parameter 1, which includes the device type (the first byte) and serial number.
            # sn = node["serial"][1:]
            name = f"{self.beamline}/flowbus/{node['serial'].lower().strip()}"
            self.__subdevices.append(name)
            class_name = "FlowBus"
            try:
                db.get_device_info(name)
                self.debug_stream(f"Device {name} already registered")
            except DevFailed:
                self.info_stream(f"Registering device {name}")

                def fill_new_device_properties(name):
                    if self.comm == Comm.Serial.value:
                        db.put_device_property(
                            name,
                            {
                                "comm": self.comm,
                                "comport": self.comport,
                                "baudrate": self.baudrate,
                                "address": node["address"],
                                "channel": node["channels"],
                            },
                        )
                    elif self.comm == Comm.RawTCP.value:
                        db.put_device_property(
                            name,
                            {
                                "comm": self.comm,
                                "hostname": self.hostname,
                                "port": self.port,
                                "address": node["address"],
                                "channel": node["channels"],
                            },
                        )

                util.create_device(class_name, name, cb=fill_new_device_properties)

        self.set_state(DevState.ON)

    @command
    def start(self):
        self.__instrument.start()

    @command
    def stop(self):
        self.__instrument.stop()

    @attribute(
        label="subdevices",
        dtype=(str,),
        max_dim_x=64,
        access=AttrWriteType.READ,
        doc="A list of subdevices (spawned for each nodes)",
    )
    def subdevices(self):
        return self.__subdevices

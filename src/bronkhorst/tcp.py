# -*- coding: utf-8 -*-
#
# RawTCP class to allow connection to a device via a serial-to-ethernet
# converter (like brainbox).

import sys
import logging
import array
import socket
import fcntl  # For ioctl
import termios  # For FIONREAD

# Get loggers
_logger = logging.getLogger(__name__)

# A Brainbox serial port class
class RawTCP:
    """A Raw TCP class that implements the serial protocol required by the propar library"""

    def __init__(self, host: str, port: int, **kwargs):
        # writing to stdout
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        _logger.addHandler(handler)

        _logger.info(f"Connecting socket to {host}:{port}")

        self._host = host
        self._port = port

        # Open the connection
        self._socket = socket.create_connection((self._host, self._port))

        _logger.debug(self._socket)

    def close(self):
        _logger.debug(f"Entering close")

    def open(self):
        _logger.debug(f"Entering open")

    def read(self, size=1) -> bytes:
        _logger.debug("Entering read")

        if not self._socket:
            raise RuntimeError("Socket not opened")

        data = self._socket.recv(size)

        _logger.debug(f"Read {data} from socket")

        return data

    def write(self, data: bytes):
        _logger.debug("Entering write")

        if not self._socket:
            raise RuntimeError("Socket not opened")

        self._socket.sendall(data)

        _logger.debug(f"Wrote {data} to socket")

    @property
    def in_waiting(self) -> int:
        _logger.debug("Entering in_waiting")

        # Return number of bytes available for reading
        amt = array.array("i", [0])
        fcntl.ioctl(self._socket, termios.FIONREAD, amt)
        nb_bytes = amt[0]

        _logger.debug(f"{nb_bytes} available")

        return nb_bytes

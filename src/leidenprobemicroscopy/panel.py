import json
import serial
from time import sleep

from tango.server import Device, attribute, command, device_property, AttrWriteType
from tango import DbDevInfo, DevState, DevFailed
from tango import Database, Util


class Instrument:
    def __init__(self, comport, baudrate) -> None:
        self.comport = comport
        self.baudrate = baudrate
        self.comm = serial.Serial(port=comport, baudrate=baudrate, timeout=1)
        self.__states = self.read_states()

    def reset_controller(self):
        self.comm.write(b"*RST")
        self.comm.close()
        max_tries = 5
        for tr in range(max_tries):
            try:
                sleep(1)
                self.comm.open(port=self.comport, baudrate=self.baudrate)
            except serial.SerialException:
                print("Could not connect in try ", tr, " of ", max_tries)
                self.close_serial()  # close comm, makes serial_address empty
            else:
                self.update_state()
                break
        else:
            raise serial.SerialException(
                "Cannot find controller anymore at port ",
                self.comport,
                ". Try initializing GasFrontPanel again",
            )

    def close_all(self):
        self.__states = self._query(b"X")

    def read_states(self):
        self.__states = self._query(b"S")

    def write_states(self, states: list):
        for i, state in enumerate(states):
            if state:
                command = bytearray(b"P\x00\x00")
                # command[1] = (button_pin[2 * i] << 4) | button_pin[(2 * i) + 1]
                # command[2] = (new_st_val[2 * i] << 4) | new_st_val[(2 * i) + 1]
                command[1] = i << 4
                command[2] = state << 4
                self._query(bytes(command))

    @property
    def idn(self):
        return self._query(b"*IDN")

    @property
    def states(self):
        return self.__states

    def _query(self, command: bytes):
        command += b"\n"
        self.comm.write(command)
        reply = self.comm.readline()
        # answer.append(self._handle_reply(reply))
        # while GasFrontPanel.comm.in_waiting > 0:
        #     reply = GasFrontPanel.comm.readline()
        #     answer.append(self._handle_reply(reply))
        return self._handle_reply(reply)

    # Functions that interpret the reply from the device:
    def _handle_reply(self, reply):
        if reply[0] == ord("S"):
            if len(reply) == 4:
                return self._raw_to_state(reply)
            else:
                return "Err: " + reply.decode() + " is not a valid status reply."
        elif reply[0] == ord("L"):
            return reply.decode()
        elif reply[0] == ord("C"):
            return "Err: " + reply.decode()
        else:
            return "Err: " + reply.decode() + " is an unknown reply."

    def _raw_to_state(self, buffer):
        register = int((buffer[1] << 8) + buffer[2])
        states = [(register & (int(1) << i)) >> i for i in range(14)]
        return states


class GasFrontPanel(Device):
    """GasFrontPanel device"""

    comport = device_property(dtype=str, default_value="COM1")
    baudrate = device_property(dtype=int, default_value=38400)

    def init_device(self):
        super(Device, self).init_device()
        self.info_stream(
            f"Configuration: comport: {self.comport} - baudrate: {self.baudrate}"
        )

        self.__instrument = Instrument(self.comport, self.baudrate)

        self.set_state(DevState.ON)

    @attribute(
        label="idn",
        dtype=str,
        access=AttrWriteType.READ,
        doc="Reads the IDN/SN parameter of the instrument",
    )
    def idn(self):
        """The IDN/SN of te instrument"""
        return self.__instrument.idn

    @attribute(
        label="states",
        dtype=(int,),
        max_dim_x=14,
        access=AttrWriteType.READ,
        doc="Reads the states of the instrument",
    )
    def states(self):
        """The state of the instrument"""
        self.__instrument.read_states()
        return self.__instrument.states

    @states.write
    def states(self, states):
        """The state of the instrument"""
        self.__instrument.write_states(states)

    @command
    def reset(self):
        """Send the RST (reset) command to the controller"""
        self.__instrument.reset_controller()

    @command
    def close_all(self):
        """Send the S (stop) command to the controller"""
        self.__instrument.close_all()

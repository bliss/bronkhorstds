# Bronkhorst Propar Tango DS

This Tango device server wraps the bronkhorst-propar API to communicate with FlowBus devices.

## Installation

### Brainbox 

If used over TPC comunincation, here the typical setup of the Brainbox ES:

![Brainbox config](assets/brainboxes.png)

### Tango

1. Register a new Device server with Jive`s server wizard.

2. Run the device

``` 
FlowBus <instance name>
```

3. Add a new `FlowBusMaster` class in Jive

Fill the properties:


| Property | Value | Documentation |
|----------|-------|---------------|
| `beamline` | `id10` | the domain part of the subdevice's tango fullname |
| `comm`     | `serial | tcp` | The type of communication local serial port or RAW TCP (e.g. for Brainboxes) |
| `port`     | `COM1 | /dev/tty1` | The serial port (for local serial communication)|
| `baudrate` | `38400` | The baudrate (for local serial communication) |
| `host`     | `brainbox.esrf.fr` | The hostname (for RAW TCP communication)|
| `port`     | `9001` | The port (for RAW TCP communication) |

4. This `FlowBusMaster` device will discover all the devices on the bus and spawn `FlowBus` subdevices for each nodes:

```
id10/flowbus/snm22219161b
id10/flowbus/snm23205135a
id10/flowbus/snm22219161d
```

The `domain/family/member` is the tango identifier where `domain` is the `beamline` property filled in before and `member` is the `idn` of the node.

## Usage

Each node in the FLOW-BUS bus is a separate device.

### Commands

| Name | In | Out | Documentation |
|------|----|-----|---------------|
| `wink` | - | - | The LED on the device blinks to identify it |
| `read_parameter` |  DDE nr [`DevLong64`] | Parameter [`DevString`] | Read a single parameter indicated by DDE nr. |
| `write_parameter` | DDE nr [`DevLong64`] | - | Write a single parameter indicated by DDE nr. |

### Attributes

| Name | Documentation |
|------|---------------|
| `idn` | The unique identifier of the device |
| `type` | Reads the sensor type of the instrument |
| `fluid` | The type of fluid (Ar, H2...) |
| `fluid_temperature` | Temperature of the fluid |
| `measure` | Measured value for direct reading (in capunits, max.= capacity) |
| `orifice` | Orifice diameter in mm |
| `pressure_inlet` | pressure inlet (upstream) of fluid in bar (for first fluid nr only) |
| `pressure_outlet` | Pressure outlet (downstream) of fluid in bar (for first fluid nr only) |
| `setpoint` | Wanted value for direct reading (in capunits, max.= capacity) |

## Release Notes

v1.1.0 - Add support for RAW TCP communication.

v1.0.0 - Initial release of FlowBus Device Server.